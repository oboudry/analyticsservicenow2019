---
title: "Analysis of MIS Incidents for year 2019"
output:
  word_document: default
  html_notebook: default
  pdf_document: default
  html_document:
    df_print: paged
---

```{r echo=FALSE}
# Load required packages
suppressMessages({
  if (!require("pacman")) install.packages("pacman")
  pacman::p_load(tidyverse, ggplot2, forcats)
  
  # Import and prepare data
  data <- read_csv("../inputs/incident.csv", locale = locale(encoding = "latin1"))
})
# Combine Summary and Description
cols <- c('Summary' , 'Description')
data$Text <- apply( data[ , cols ] , 1 , paste , collapse = " " )

bs = data %>% 
  group_by(`Business service`) %>% 
  summarize(Count = n()) %>% 
  arrange(desc(Count)) %>%
  mutate(Cumulative=cumsum(Count)) %>%
  mutate(Pareto=cumsum(Count)/sum(Count)) %>%
  mutate(`Business service` = forcats::fct_inorder(`Business service`)) %>%
  filter(Cumulative <= 0.8*sum(Count))

ggplot(bs, aes(x=`Business service`)) +
  geom_col(aes(y=Count), fill='blue') +
  geom_point(aes(y=Cumulative), color = rgb(0, 1, 0), pch=16, size=1) +
  geom_path(aes(y=Cumulative, group=1), colour="slateblue1", lty=3, size=0.9) +
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5)) +
  labs(title = "Pareto Plot", subtitle = "Incidents by Business Service",
       x = 'Business Service', y = 'Count') +
  ggtitle("Ticket by Business Service")
```


```{r echo=FALSE}
# Plot top 10 business services
bs <- data %>%
  group_by(`Business service`) %>%
  summarise(Count = n()) %>%
  mutate(`Business service` = fct_reorder(`Business service`, -Count)) %>%
  arrange(`Business service`)

ggplot(data = bs[1:10,], aes(x = `Business service`, y = Count)) + 
  geom_col() + coord_flip() +
  ggtitle("Top 10 business services")
```

```{r echo=FALSE}
ad <- data %>%
  filter(`Business service` == "Active Directory") %>%
  group_by(Category) %>%
  summarise(Count = n()) %>%
  mutate(Category = fct_reorder(Category, -Count)) %>%
  arrange(Category)

ggplot(data = ad[1:10,], aes(x = Category, y = Count)) + 
  geom_col() + coord_flip() +
  ggtitle("Active Directory")
```

```{r echo=FALSE}
adadm <- data %>%
  filter(`Business service` == "Active Directory" & Category == 'Account Admin') %>%
  group_by(Subcategory) %>%
  summarise(Count = n()) %>%
  mutate(Subcategory = fct_reorder(Subcategory, -Count)) %>%
  arrange(Subcategory)

ggplot(data = adadm[1:10,], aes(x = Subcategory, y = Count)) +
  geom_col() + coord_flip() + 
  ggtitle("AD Account Admin")
```

```{r echo=FALSE}
adsrv <- data %>%
  filter(`Business service` == "Active Directory" & Category == 'Service Request') %>%
  group_by(Subcategory) %>%
  summarise(Count = n()) %>%
  mutate(Subcategory = fct_reorder(Subcategory, -Count)) %>%
  arrange(Subcategory)

ggplot(data = adsrv[1:10,], aes(x = Subcategory, y = Count)) + 
  geom_col() + coord_flip() +
  ggtitle("AD Service Request")
```

```{r echo=FALSE}
# Stacked bar plot
top4 <- data %>%
  filter(`Business service` %in% c('Active Directory', 'Endpoint Device', 'Site Specific Application', 'Email')) %>%
  group_by(`Business service`, Subcategory) %>%
  summarise(Count = n()) %>%
  filter(Count >= 1000)

ggplot(data = top4, aes(x = `Business service`, fill = Subcategory, y = Count)) + 
  geom_bar(position="stack", stat="identity") +
  ggtitle("Top 4 Business Services (w/o SAP) 58% of tickets") +
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5))

```

```{r echo=FALSE}
ggplot(data, aes(x = Priority)) + 
  geom_bar() + ggtitle("Tickets by Priority") + 
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5))
```

```{r echo=FALSE}
affected_cis = sort(table(data$`Affected CI`), decreasing = T)
frequent_cis = affected_cis[affected_cis > 10]
other_cis = c(Other=sum(affected_cis[affected_cis <= 10]))
undef_cis = c(Undefined=sum(is.na(data$`Affected CI`)))
affected_cis = c(frequent_cis, other_cis, undef_cis)
affected_cis = data.frame(CI = names(affected_cis), Count = affected_cis)
ggplot(affected_cis, aes(x = CI, y = Count)) +
  geom_col() + coord_flip() + ggtitle("Assigned CIs")
```


```{r echo=FALSE}
ggplot(data, aes(x = Source)) + 
  geom_bar() + ggtitle("Ticket Source") + 
  theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0.5))
```

```{r echo=FALSE}
ggplot(data, aes(x = Status)) + geom_bar() + ggtitle("Ticket status") + coord_flip()
```

