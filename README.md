# ServiceNow Analytics 2019

Stats for 2019 incidents

## Getting Started

Export all 2019 incidents to a CSV file, making sure fields "Business service", "Category", and "Subcategory" are included. Then upload file as `outputs/incident.csv`, and run scripts.


## Authors

* **Olivier Boudry** - *Various analysis*

## License

Copyright 2019 Olivier Boudry

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
